
import concurrent.futures as cf
import socket
import paramiko

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())


output_ips = open('output.txt', 'r', encoding='utf-8').read().split('\n')


def checker(hostname):
        try:
            ssh.connect(hostname=hostname, username='root', password='123', port=22, timeout=0.5)

        except paramiko.ssh_exception.AuthenticationException:
            print("Connected to " + hostname)

            with open("good.txt",'a') as file_handler:
                file_handler.write(hostname+'\n')

        except paramiko.ssh_exception.NoValidConnectionsError:
            print('Many connect error')

        except paramiko.ssh_exception.SSHException:
            print('exist session')

        except ConnectionResetError:
            print('Host lost')
        except socket.timeout:
            return

        except Exception:
            print('banned')
        except:
            return


with cf.ThreadPoolExecutor(max_workers=1000) as count_Thread:
    for _ in count_Thread.map(checker, output_ips):
        pass

