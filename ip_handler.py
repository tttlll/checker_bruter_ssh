import itertools
ips = open('input.txt','r').read().split('\n')
out_ips = open('output.txt', 'a')


def create_ips(first, second):
    flag = True
    predlast = None
    last = None

    for i in range(4):

        if first[i] != second[i]:

            if i == 3:
                last = range(int(first[i]), int(second[i])+1)
            if i == 2:
                flag = False
                predlast = range(int(first[i]), int(second[i])+1)
                last = range(0, 256)
                break
    if last is None:
        return
    if flag:
        for ip in last:
            out_ips.write('{}.{}.{}.{}\n'.format(first[0], first[1], first[2], ip))
    else:
        for ip in list(itertools.product(predlast, last)):
            out_ips.write('{}.{}.{}.{}\n'.format(first[0], first[1], ip[0], ip[1]))

